FROM maven:3.6.3-jdk-8-slim AS build
COPY src /home/app/src
COPY pom.xml /home/app
RUN mvn -f /home/app/pom.xml clean package

FROM openjdk:8-jdk-alpine
EXPOSE 8088
COPY --from=build /home/app/target/*.jar /usr/local/lib/app.jar
ENTRYPOINT ["sh", "-c", "java -jar /usr/local/lib/app.jar"]
