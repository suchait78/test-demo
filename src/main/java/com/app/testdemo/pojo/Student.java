package com.app.testdemo.pojo;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Student {

	//@JsonProperty("student_id")
	private Integer studentId;
	
	//@JsonProperty("student_name")
	private String studentName;
	
	//@JsonProperty("student_dept")
	private String studentDept;
	
	public Student() {
		
	}
	
	public Student(Integer studentId, String studentName, String studentDept) {
		super();
		this.studentId = studentId;
		this.studentName = studentName;
		this.studentDept = studentDept;
	}

	public Integer getStudentId() {
		return studentId;
	}

	public void setStudentId(Integer studentId) {
		this.studentId = studentId;
	}

	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public String getStudentDept() {
		return studentDept;
	}

	public void setStudentDept(String studentDept) {
		this.studentDept = studentDept;
	}

	

}
