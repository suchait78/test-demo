package com.app.testdemo.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.testdemo.pojo.Student;

@RestController
@RequestMapping("/v2/test-demo/")
public class StudentController {

	private static List<Student> studentsList = new ArrayList<>();

	static {

		studentsList.add(new Student(12, "test12", "CSE"));
		studentsList.add(new Student(13, "test12", "CSE"));
		studentsList.add(new Student(14, "test14", "CSE"));
		studentsList.add(new Student(15, "test15", "CSE"));

	}

	@PostMapping("/create/student")
	public EntityModel<Student> postStudent(@RequestBody Student student) {
		
		studentsList.add(student);
		EntityModel<Student> resource = EntityModel.of(student);

		WebMvcLinkBuilder linktoAllStudents = WebMvcLinkBuilder
				.linkTo(WebMvcLinkBuilder.methodOn(this.getClass()).getAllStudents());
		resource.add(linktoAllStudents.withRel("Get-All-Students"));
		
		WebMvcLinkBuilder linktoStudent = WebMvcLinkBuilder
				.linkTo(WebMvcLinkBuilder.methodOn(this.getClass()).getStudentById(student.getStudentId()));
		resource.add(linktoStudent.withRel("Get-student"));
		
		return resource;
	}
	
	@GetMapping("/get/all/students")
	public List<Student> getAllStudents() {

		return studentsList;
	}

	@GetMapping("/get/student/{id}")
	public EntityModel<Student> getStudentById(@PathVariable Integer id) {

		Student student = studentsList.stream().filter(studentObj -> studentObj.getStudentId().equals(id)).findAny()
				.get();

		EntityModel<Student> resource = EntityModel.of(student);

		WebMvcLinkBuilder linkto = WebMvcLinkBuilder
				.linkTo(WebMvcLinkBuilder.methodOn(this.getClass()).getAllStudents());
		resource.add(linkto.withRel("Get-All-Students"));

		return resource;
	}

	@DeleteMapping("/delete/student/{id}")
	public EntityModel<Student> deleteStudentById(@PathVariable Integer id) {

		Student targetStudent = studentsList.stream().filter(studentObj -> studentObj.getStudentId().equals(id))
				.findAny().get();
		studentsList.remove(targetStudent);

		EntityModel<Student> resource = EntityModel.of(targetStudent);

		WebMvcLinkBuilder linktoAllStudents = WebMvcLinkBuilder
				.linkTo(WebMvcLinkBuilder.methodOn(this.getClass()).getAllStudents());
		resource.add(linktoAllStudents.withRel("Get-All-Students"));
		
		return resource;

	}

}
