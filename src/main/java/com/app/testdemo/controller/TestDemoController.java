package com.app.testdemo.controller;

import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.app.testdemo.pojo.Student;

import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;

@RestController
@RequestMapping("/test-demo")
public class TestDemoController {

	static final Logger LOG = LoggerFactory.getLogger(TestDemoController.class);
	
	@Value("${SERVICE_DEMO_SERVICE_HOST:http://localhost}")
	private String serviceDemoHostUrl;
	
	private static final String HOST_NAME = "HOSTNAME";
    private static final String SERVICE = "serviceDemo";

	private static final String DEFAULT_ENV_INSTANCE_GUID = "UNKNOWN";
	
	@Value("${" + HOST_NAME + ":" + DEFAULT_ENV_INSTANCE_GUID + "}")
	private String hostName;
	
	@GetMapping("/get/student-bean")
	public Student getStudentBean() {
				
		Random random = new Random();
		return new Student(Math.abs(random.nextInt()), "Suchait", "CSE");
		
	}
	
	@GetMapping("/")
	public String rootPath() {
		return "Application is running!!";
	}
	
	@GetMapping("/host")
	public String getValue() {
		return serviceDemoHostUrl;
	}
	
	@GetMapping("/get/student/other-service")
	public Student callServiceDemo() {
		
		System.out.println("Service-demo URL : "+serviceDemoHostUrl);
		
		ResponseEntity<Student> studentResponse = 
				new RestTemplate().getForEntity(serviceDemoHostUrl + ":8091/service-demo/get/student", Student.class);
		
		if(!studentResponse.getStatusCode().is2xxSuccessful()) {
			//throw error
		}
		
		return studentResponse.getBody();
	}
	
	@GetMapping("/get/env/details")
	public String getEnvDetails() {
		
		return hostName + " v1 " + hostName.substring(hostName.length()-5);
	}


	@GetMapping("/v1/get/student/other-service")
	@CircuitBreaker(name = SERVICE, fallbackMethod = "getDefaultStudent")
	public Student callServiceDemoAndUseCircuitBreaker() {
		
		System.out.println("Service-demo URL : "+serviceDemoHostUrl);
		
		ResponseEntity<Student> studentResponse = 
				new RestTemplate().
				getForEntity(serviceDemoHostUrl + ":8091/service-demo/get/student", Student.class);
		
		if(!studentResponse.getStatusCode().is2xxSuccessful()) {
			//throw error
		}
		
		return studentResponse.getBody();
	}
	
	
	public Student getDefaultStudent(Throwable t) {
		return new Student(15,"default-student","CSE");
	}
	
}
